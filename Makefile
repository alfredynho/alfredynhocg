HOST := hostname -I

run:
	@echo "------------------------> Running Server <------------------------"
	php artisan serve

migrate:
	php artisan migrate

migrate-status:
	php artisan migrate:status


migrate-reset:
	php artisan migrate:reset


migrate-refresh:
	php artisan migrate:refresh

clear-laravel:
	php artisan cache:clear

bot-clear:
	php artisan config:cache
	php artisan config:clear

rd:
	npm run dev

install:
	@echo "Installing all system dependencies using apt-get"
	rm -rf .env
	sudo bash install.sh
	sudo chmod 777 .env
	php artisan config:clear
	sudo php artisan key:generate
	php artisan migrate

db:
	@echo "Options Database"
	sudo scripts/database.sh

mailserver:
	./tools/bin/mailhog &
	@echo "MailHog opened ..."

options:
	@echo
	@echo ----------------------------------------------------------------------
	@echo "   >>>>>                 Openbackend               <<<<<   "
	@echo ----------------------------------------------------------------------
	@echo
	@echo "   - install     SETTINGS=[settings]    Install App and their dependencies"
	@echo "   - superuser   SETTINGS=[settings]    Create a super user in production"
	@echo "   - serve       SETTINGS=[settings]    Serve project for development"
	@echo "   - mail_server SETTINGS=[settings]    Open the Development Mail Server"
	@echo "   - shell       SETTINGS=[settings]    Run laravel in shell mode for development"
	@echo "   - test        SETTINGS=[settings]    Run laravel test cases"
	@echo "   - constance   SETTINGS=[settings]    settings laravel contance"
	@echo "   - sudo service apache2 restart    settings Laravel contance"
	@echo "   - sudo /etc/init.d/mysql restart    settings Laravel contance"
	@echo
	@echo ----------------------------------------------------------------------

clean_mode:
	rm -rf node_modules
	rm -rf static/dist

lint:
	@npm run lint --silent

dump:
	php artisan dump-server

clear:
	php artisan cache:clear
	php artisan config:cache
	php artisan route:clear
	php artisan view:clear

tunnel:
	./ngrok http 8000

img:
	php artisan storage:link

version:
	php artisan --version

production:
	cp -r ./deploy/.env.production .env

dev:
	cp -p ./deploy/.env.dev .env

share:
	@echo ----------------------------------------------------------------------
	@echo "   >>>>>  Listo para compartir Proyecto        <<<<<   "
	$(HOST)
	@echo ----------------------------------------------------------------------
	php artisan serve --host=0.0.0.0 --port=8000


seed:
	php artisan db:seed --class=DbSeeder

store:
	mkdir Convenios
	mkdir Documents
	mkdir Cursos
	curl -o Convenios/soboce.jpg https://res.cloudinary.com/alfredynho-inso/image/upload/v1583676571/inso/soboce_wwlwit.jpg
	curl -o Convenios/gamlp.jpg https://res.cloudinary.com/alfredynho-inso/image/upload/v1583676571/inso/gamlp_lafyod.jpg
	curl -o Convenios/ende.png https://res.cloudinary.com/alfredynho-inso/image/upload/v1583676571/inso/ende_nyhmna.png
	curl -o Convenios/maldonado.png https://res.cloudinary.com/alfredynho-inso/image/upload/v1583676571/inso/maldonado_nqpytf.png
	curl -o Convenios/epiroc.jpg https://res.cloudinary.com/alfredynho-inso/image/upload/v1583676570/inso/epiroc_uptijp.jpg
	curl -o Documents/normativa_inso2020.pdf https://res.cloudinary.com/alfredynho-inso/image/upload/v1583676561/inso/Resumen-Ejecutivo-2020_vnosbx.pdf
	curl -o Cursos/salud-publica.png https://res.cloudinary.com/due8e2c3a/image/upload/v1583860091/INSO/salud-publica.png
	curl -o Cursos/neurociencia.png https://res.cloudinary.com/due8e2c3a/image/upload/v1583860087/INSO/neurociencia.png
	curl -o Cursos/investigacion.png https://res.cloudinary.com/due8e2c3a/image/upload/v1583860088/INSO/investigacion.png
	curl -o Cursos/sistemas.png https://res.cloudinary.com/due8e2c3a/image/upload/v1583860088/INSO/sistemas.png
	curl -o Cursos/salud-ocupacional.png https://res.cloudinary.com/due8e2c3a/image/upload/v1583860087/INSO/salud-ocupacional.png
	curl -o Cursos/coronavirus.png https://res.cloudinary.com/due8e2c3a/image/upload/v1586297132/INSO/coronavirus.png
	curl -o Cursos/psicologiaensalud.png https://res.cloudinary.com/due8e2c3a/image/upload/v1586297132/INSO/neurociencia.png
	cp -r Cursos public/storage
	cp -r Convenios public/storage
	cp -r Documents public/storage
	rm -rf Convenios
	rm -rf Documents
	rm -rf Cursos

refresh:
	php artisan migrate:refresh

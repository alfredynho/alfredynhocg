<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@alfredynho</title>
    <link rel="shortcut icon" href="{{ asset('favicon.png')}}" type="image/x-icon" />
    <meta name="description" content="Alfredo Callizaya Gutierrez">
    <meta name="keywords" content="Python, PHP , Django, Laravel">
    @laravelPWA

    @include('partials.styles')

    @section('extracss')

    @show

  </head>
  <body>



  <!-- START: header -->
      @include('partials.header')
  <!-- END: header -->

  <!-- START: section -->
  <section class="probootstrap-section probootstrap-feature-first">
    <div class="container">
      <div class="row mb70" style="margin-top: -120px;">
        <div class="col-md-4 probootstrap-animate">
          <div class="probootstrap-box">
              <div class="icon"><i class="icon-presentation"></i></div>
              <h3>Business presentation</h3>
              <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
          </div>
        </div>
        <div class="col-md-4 probootstrap-animate">
          <div class="probootstrap-box">
              <div class="icon"><i class="icon-bargraph"></i></div>
              <h3>Business growth</h3>
              <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
          </div>
        </div>
        <div class="col-md-4 probootstrap-animate">
          <div class="probootstrap-box">
              <div class="icon"><i class="icon-megaphone2"></i></div>
              <h3>Standout from the crowd</h3>
              <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 col-md-offset-4 probootstrap-animate">
          <p class="text-center">
            <a href="#" class="btn btn-primary btn-lg btn-block" role="button">Get started</a>
          </p>
        </div>
      </div>
    </div>
  </section>
  <!-- END: section -->


  <!-- START: section -->
    @yield('content')
  <!-- END: section -->

  <!-- START: footer -->
      @include('partials.footer')
  <!-- END: footer -->

  @include('partials.js')

  @section('extrajs')

  @show

  </body>
</html>

<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet"> 
<link rel="stylesheet" href="{{ asset('css/styles-merged.css')}}">
<link rel="stylesheet" href="{{ asset('css/style.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/custom.css')}}">
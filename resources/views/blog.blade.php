@extends('base')

@section('content')

<!-- START: section -->
<section class="probootstrap-section">
  <div class="container">
    
    <div class="row">
      <div class="col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
        <div class="probootstrap-block-image">
          <figure><img src="img/img_3.jpg" alt="Free Bootstrap Template by uicookies.com"></figure>
          <div class="text">
            <span class="date">June 29, 2017</span>
            <h3><a href="#">Laboriosam Quod Dignissimos</a></h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
            <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
        <div class="probootstrap-block-image">
          <figure><img src="img/img_2.jpg" alt="Free Bootstrap Template by uicookies.com"></figure>
          <div class="text">
            <span class="date">June 29, 2017</span>
            <h3><a href="#">Laboriosam Quod Dignissimos</a></h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
            <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
          </div>
        </div>
      </div>
      <div class="clearfix visible-sm-block"></div>
      <div class="col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
        <div class="probootstrap-block-image">
          <figure><img src="img/img_3.jpg" alt="Free Bootstrap Template by uicookies.com"></figure>
          <div class="text">
            <span class="date">June 29, 2017</span>
            <h3><a href="#">Laboriosam Quod Dignissimos</a></h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
            <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
          </div>
        </div>
      </div>
      <div class="clearfix visible-md-block"></div>
      <div class="col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
        <div class="probootstrap-block-image">
          <figure><img src="img/img_2.jpg" alt="Free Bootstrap Template by uicookies.com"></figure>
          <div class="text">
            <span class="date">June 29, 2017</span>
            <h3><a href="#">Laboriosam Quod Dignissimos</a></h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
            <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
          </div>
        </div>
      </div>
      <div class="clearfix visible-sm-block"></div>
      <div class="col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
        <div class="probootstrap-block-image">
          <figure><img src="img/img_3.jpg" alt="Free Bootstrap Template by uicookies.com"></figure>
          <div class="text">
            <span class="date">June 29, 2017</span>
            <h3><a href="#">Laboriosam Quod Dignissimos</a></h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
            <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12 probootstrap-animate">
        <div class="probootstrap-block-image">
          <figure><img src="img/img_2.jpg" alt="Free Bootstrap Template by uicookies.com"></figure>
          <div class="text">
            <span class="date">June 29, 2017</span>
            <h3><a href="#">Laboriosam Quod Dignissimos</a></h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto provident qui tempore natus quos quibusdam soluta at.</p>
            <p><a href="#" class="link-with-icon">Learn More <i class=" icon-chevron-right"></i></a></p>
          </div>
        </div>
      </div>
      <div class="clearfix visible-sm-block"></div>
    </div>
    
  </div>
</section>
<!-- END: section -->

@endsection('content')

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $year_services = date("Y")-1944;

        $data=array('menu'=>'providers','title_template'=>'Incos La Paz');
    
        return view('index', $data)->with(compact('year_services'));

    }
}
